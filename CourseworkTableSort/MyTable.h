#pragma once
#include "Scroll.h"
#include <Windows.h>
#include <vector>
#include <string>

using namespace std;

class MyTable {
private:
	vector<string> columns;
	vector<vector<string>> rows;
	vector<vector<string>> oldRows;

	char* filename;
	char* saveFilename;

	vector<int> clicksVec;
	vector<int> widthColumns;
	int heightCol;

	int totalWidth;
	int totalHeight;

	HFONT hFont;
	HFONT hHeaderFont;
	HPEN hPen;

	COLORREF columnsColor;
	COLORREF columnNum;
public:
	MyTable();
	MyTable(vector<string> columns, vector<vector<string>> rows);
	MyTable(char* filename);

	void ReadTable();
	void WriteTable(bool dialog);

	void Show(HWND hWnd, HDC hdc, Scroll* scroll);

	void SortByColumn(int col);
	pair<int, int> GetPosOfCellByCoords(int x, int y);

	int GetMaxWidthOfColumn(int i);
	bool SelectFile(HWND hWnd, bool open);

	char* GetCurrentFileName();

	void InitTools();

	~MyTable();
};

bool CompareDesc(vector<string> v1, vector<string> v2, int col);
bool CompareAsc(vector<string> v1, vector<string> v2, int col);
bool CompareAscNum(vector<string> v1, vector<string> v2, int col);
bool CompareDescNum(vector<string> v1, vector<string> v2, int col);