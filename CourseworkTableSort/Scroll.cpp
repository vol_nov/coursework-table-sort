#include "Scroll.h"

Scroll::Scroll(HWND hWnd, int maxW, int maxH) {
	counterX = 0;
	counterY = 0;

	maxWidth = maxW;
	maxHeight = maxH;

	dx = (maxWidth) / 100;
	dy = (maxHeight) / 100;

	SCROLLINFO info;
	GetScrollInfo(hWnd, SB_VERT, &info);
	info.nMin = 0;
	info.nMax = 100;
	SetScrollInfo(hWnd, SB_VERT, &info, true);

	GetScrollInfo(hWnd, SB_HORZ, &info);
	info.nMin = 0;
	info.nMax = 100;
	SetScrollInfo(hWnd, SB_HORZ, &info, true);
}

void OnVScroll(Scroll* scroll, HWND hWnd, WPARAM wParam, LPARAM lParam) {
	switch (LOWORD(wParam))
	{
	case SB_TOP:
		scroll->counterY = 0;
		break;

	case SB_LINEUP:
		if (scroll->counterY > 0)
			scroll->counterY -= 1;
		break;

	case SB_THUMBPOSITION:
		scroll->counterY = HIWORD(wParam);
		break;

	case SB_THUMBTRACK:
		scroll->counterY = HIWORD(wParam);
		break;

	case SB_LINEDOWN:
		if (scroll->counterY < 100)
			scroll->counterY += 1;
		break;

	case SB_BOTTOM:
		scroll->counterY = 100;
		break;

	case SB_ENDSCROLL:
		break;
	}

	SetScrollPos(hWnd, SB_VERT, scroll->counterY, TRUE);
	InvalidateRect(hWnd, NULL, TRUE);
}

void OnHScroll(Scroll* scroll, HWND hWnd, WPARAM wParam, LPARAM lParam) {
	switch (LOWORD(wParam))
	{
	case SB_LEFT:
		scroll->counterX = 0;
		break;

	case SB_LINELEFT:
		if (scroll->counterX > 0)
			scroll->counterX -= 1;
		break;

	case SB_THUMBPOSITION:
		scroll->counterX = HIWORD(wParam);
		break;

	case SB_THUMBTRACK:
		scroll->counterX = HIWORD(wParam);
		break;

	case SB_LINERIGHT:
		if (scroll->counterX < 100)
			scroll->counterX += 1;
		break;

	case SB_RIGHT:
		scroll->counterX = 100;
		break;

	case SB_ENDSCROLL:
		break;
	}

	SetScrollPos(hWnd, SB_HORZ, scroll->counterX, TRUE);
	InvalidateRect(hWnd, NULL, TRUE);
}