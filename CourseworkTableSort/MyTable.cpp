#include "MyTable.h"
#include <fstream>
#include <climits>

bool(*Compare)(vector<string> v1, vector<string> v2, int col);

MyTable::MyTable() {
	filename = nullptr;
	saveFilename = nullptr;

	InitTools();
}

MyTable::MyTable(vector<string> columns, vector<vector<string>> rows) {
	filename = nullptr;
	for (auto it = columns.begin(); it != columns.end(); it++) {
		this->columns.push_back(*it);
	}

	for (auto it = rows.begin(); it != rows.end(); it++) {
		this->rows.push_back(*it);
	}
	saveFilename = nullptr;

	InitTools();
}

MyTable::MyTable(char* filename) {
	this->filename = new char[strlen(filename) + 1];
	strcpy(this->filename, filename);
	saveFilename = nullptr;
	InitTools();
	ReadTable();
}

void MyTable::InitTools() {
	hFont = CreateFont(20, 8, 0, 0, FW_MEDIUM, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
		CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, VARIABLE_PITCH, TEXT("Calibri"));

	hHeaderFont = CreateFont(20, 8, 0, 0, FW_MEDIUM, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
		CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, VARIABLE_PITCH, TEXT("Arial"));

	hPen = CreatePen(PS_SOLID, 2, RGB(0, 0, 0));

	columnsColor = RGB(213, 216, 220);
	columnNum = RGB(144,148,151);
}

void MyTable::ReadTable() {
	columns.clear();
	clicksVec.clear();
	rows.clear();
	
	//���������� �������
	//�� ������������� ������ �������� ������
	//�������: ������ ����������
	columns.push_back("#");

	ifstream ifs(filename);
	string line;
	getline(ifs, line);


	//������� ������ �������
	char* temp = new char[strlen(line.c_str()) + 1];
	strcpy(temp, line.c_str());

	char* delim = "\t";

	clicksVec.push_back(0);

	char* split = strtok(temp, delim);
	while (split != NULL) {
		columns.push_back(string(split));
		clicksVec.push_back(0);
		split = strtok(NULL, delim);
	}

	delete temp;

	//������� �����
	int counter = 1;
	char convert_buf[8];
	while (getline(ifs, line)) {
		vector<string> row;

		_itoa(counter, convert_buf, 10);

		row.push_back(string(convert_buf));

		temp = new char[strlen(line.c_str()) + 1];
		strcpy(temp, line.c_str());

		split = strtok(temp, delim);
		while (split != NULL) {
			row.push_back(string(split));
			split = strtok(NULL, delim);
		}

		rows.push_back(row);
		
		counter++;
	}

	oldRows = rows;

	ifs.close();
}

void MyTable::WriteTable(bool dialog) {
	char* save;
	if (dialog)
		save = saveFilename;
	else
		save = filename;

	//��������� ������� ������
	columns.erase(columns.begin());

	for (auto it = rows.begin(); it != rows.end(); it++) {
		(*it).erase((*it).begin());
	}

	ofstream ofs;
	ofs.open(save, std::ofstream::out | std::ofstream::trunc);

	//�� ����, ���� ������ ������� �����, ��� ������� ������� �������

	for (auto it = columns.begin(); it != columns.end(); it++) {
		if ((*it).size() == 0)
			ofs << " " << '\t';
		else 
			ofs << (*it).c_str() << '\t';
	}
	ofs << '\n';

	for (auto it = rows.begin(); it != rows.end(); it++) {
		auto row = (*it);
		for (auto it1 = row.begin(); it1 != row.end(); it1++) {
			if ((*it1).size() == 0)
				ofs << " " << '\t';
			else
				ofs << (*it1).c_str() << '\t';
		}
		ofs << '\n';
	}

	ofs.close();
}

void MyTable::Show(HWND hWnd, HDC hdc, Scroll* scroll) {
	//��������� �������

	if (columns.size() == 0)
		return;

	SelectObject(hdc, hFont);
	SelectObject(hdc, hPen);

	//³������ � �������� �� �������� � ����������
	int cellHorMargin = 30, cellVerMargin = 15;
	widthColumns.clear();

	SIZE size;

	totalWidth = 1;
	totalHeight = 1;

	for (int i = 0; i < columns.size(); i++) {
		int length = GetMaxWidthOfColumn(i);

		if (length == 0) {
			int min = INT_MAX;
			for (int j = 0; j < i; j++) {
				if (columns.at(j).size() < min && 
					columns.at(j).size() != 0)
					min = columns.at(j).size();
			}
			length = min;
		}
		

		char* len_str = new char[length + 1];

		memset(len_str, 'a', length);
		len_str[length] = '\0';

		//��������� ������� � ������ ������� �������
		//GetTextExtentPoint32A ������� ����� ������������ ��� ��������� ������
		GetTextExtentPoint32A(hdc, len_str, length, &size);
		heightCol = size.cy + cellVerMargin + 1;

		//������ ������� � ������ ������� �������, 
		//��� ���� ��������� �������, �� ��� �������� ����������

		widthColumns.push_back(size.cx + cellHorMargin);

		totalWidth += (size.cx + cellHorMargin + 1);

		delete len_str;
	}

	totalHeight = (size.cy + cellVerMargin + 1) * (rows.size() + 1);

	RECT toFillColumns;
	toFillColumns.left = 2;
	toFillColumns.top = 2;
	toFillColumns.right = totalWidth;
	toFillColumns.bottom = size.cy + cellVerMargin + 2;

	HBRUSH hBrush = CreateSolidBrush(columnsColor);
	FillRect(hdc, &toFillColumns, hBrush);

	RECT wRect;
	GetWindowRect(hWnd, &wRect);

	//����������� ���� ��� ��������
	scroll->maxWidth = totalWidth - (wRect.right - wRect.left) + 35;
	scroll->maxHeight = totalHeight - (wRect.bottom - wRect.top) + heightCol * 2;
	scroll->dx = ceil(scroll->maxWidth / 100.0); //������� �� �(�������)
	scroll->dy = ceil(scroll->maxHeight / 100.0); //������� �� Y(�������)

    //������� �� � � �������
	int scrollDx = (wRect.right - wRect.left < totalWidth) ? ((double)scroll->maxWidth*(scroll->counterX) / -100.0) : 0;
	//������� �� Y � �������
	int scrollDy = (wRect.bottom - wRect.top < totalHeight) ? ((double)scroll->maxHeight*(scroll->counterY) / -100.0) : 0;
	//³��������� ����� �������
	MoveToEx(hdc, 1, 1, NULL);
	LineTo(hdc, 1, totalHeight + scrollDy + 3);
	LineTo(hdc, totalWidth + scrollDx, totalHeight + scrollDy + 3);
	LineTo(hdc, totalWidth + scrollDx, 1);
	LineTo(hdc, 1, 1);

	RECT rectPos;
	//³���������� �������
	rectPos.left = 2 + scrollDx;
	rectPos.right = widthColumns.at(0) + rectPos.left;
	rectPos.top = scrollDy + 3;
	rectPos.bottom = rectPos.top + (heightCol);

	SelectObject(hdc, hHeaderFont);

	SetBkMode(hdc, OPAQUE);
	SetBkColor(hdc, columnsColor);
	DrawText(hdc, columns.at(0).c_str(), columns.at(0).size(), &rectPos, DT_CENTER | DT_VCENTER);

	for (int i = 1; i < columns.size(); i++) {

		rectPos.left += widthColumns.at(i - 1) + 1;
		rectPos.right += widthColumns.at(i) + 1;
		
		DrawText(hdc, columns.at(i).c_str(), columns.at(i).size(), &rectPos, DT_CENTER | DT_VCENTER);

		MoveToEx(hdc, rectPos.left, 1, NULL);
		LineTo(hdc, rectPos.left, totalHeight + 1);
	}
	
	SetBkMode(hdc, TRANSPARENT);

	SelectObject(hdc, hFont);

	hBrush = CreateSolidBrush(columnNum);
	//³���������� ����� �������
	for (int i = 0; i < rows.size(); i++) {
		rectPos.left = 2 + scrollDx;
		rectPos.right = widthColumns.at(0) + rectPos.left;
		rectPos.top = 2 + scrollDy + (heightCol) * (i + 1);
		rectPos.bottom = rectPos.top + (heightCol);
		
		FillRect(hdc, &rectPos, hBrush);

		SetBkMode(hdc, OPAQUE);
		SetBkColor(hdc, columnNum);
		
		DrawText(hdc, rows.at(i).at(0).c_str(), rows.at(i).at(0).size(), &rectPos, DT_RIGHT | DT_VCENTER);

		SetBkMode(hdc, TRANSPARENT);

		for (int j = 1; j < rows.at(i).size(); j++) {
			rectPos.left += widthColumns.at(j - 1) + 1;
			rectPos.right += widthColumns.at(j) + 1;

			DrawText(hdc, rows.at(i).at(j).c_str(), rows.at(i).at(j).size(), &rectPos, DT_CENTER | DT_VCENTER);
		}

		/*MoveToEx(hdc, 2, rectPos.bottom, NULL);
		LineTo(hdc, totalWidth + scrollDx, rectPos.bottom);*/

		MoveToEx(hdc, 2, rectPos.top, NULL);
		LineTo(hdc, totalWidth + scrollDx, rectPos.top);
	}


}

int MyTable::GetMaxWidthOfColumn(int i) {

	if (i < 0 || i > columns.size() - 1)
		return -1;

	int maxWidth = columns.at(i).size();

	for (auto it = rows.begin(); it != rows.end(); it++) {
		if ((*it).at(i).size() > maxWidth)
			maxWidth = (*it).at(i).size();
	}

	return maxWidth;
}

void MyTable::SortByColumn(int col) {
	int clicks = clicksVec[col];
	if (clicks == 2) {
		rows = oldRows;
		clicksVec[col] = 0;
	}
	else {
		if (clicks == 0) {
			
			if (col == 0) {
				Compare = &CompareAscNum;
			}
			else {
				Compare = &CompareAsc;
			}
			clicksVec[col] = 1;
		}
		else {
			if (col == 0) {
				Compare = &CompareDescNum;
			}
			else {
				Compare = &CompareDesc;
			}

			clicksVec[col] = 2;
		}
		for (int i = 0; i < rows.size(); i++) {
			for (int j = i + 1; j < rows.size(); j++) {
				if ((*Compare)(rows.at(i), rows.at(j), col)) {
					vector<string> temp = rows.at(i);
					rows.at(i) = rows.at(j);
					rows.at(j) = temp;
				}
			}
		}
	}
}

bool MyTable::SelectFile(HWND hWnd, bool open) {
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.hInstance = GetModuleHandle(NULL);
	
	char temp[255] = "";
	ofn.lpstrFile = temp;
	ofn.nMaxFile = sizeof(temp);

	if (open) {

		if (GetOpenFileName(&ofn) == TRUE) {
			if (filename != nullptr)
				delete[] filename;
			filename = new char[strlen(temp) + 1];;
			strcpy(filename, temp);
			return true;
		}
		else {
			return false;
		}
	}
	else {
		if (GetSaveFileName(&ofn) == TRUE) {
			if (saveFilename != nullptr)
				delete[] saveFilename;
			saveFilename = new char[strlen(temp) + 1];;
			strcpy(saveFilename, temp);
			return true;
		}
		else {
			return false;
		}
	}
}

pair<int, int> MyTable::GetPosOfCellByCoords(int x, int y) {
	int i = -1, j = -1;

	if (x > totalWidth || y > totalHeight) {
		return pair<int, int>(i, j);
	}
	else {
		i = y / (heightCol + 1);

		if (x > 0 && x < widthColumns.at(0))
			return pair<int, int>(i, 0);

		int left = widthColumns.at(0);

		for (int k = 1; k < widthColumns.size(); k++) {
			if (x > left && x < left + widthColumns.at(k))
				return pair<int, int>(i, k);
			left += widthColumns.at(k);
		}
	}

	return pair<int, int>(-1, -1);
}

char* MyTable::GetCurrentFileName() {
	return filename;
}

MyTable::~MyTable() {
	if (filename != nullptr)
		delete filename;

	if (saveFilename != nullptr)
		delete saveFilename;

	saveFilename = nullptr;
	filename = nullptr;
}

bool CompareDesc(vector<string> v1, vector<string> v2, int col) {
	return v1.at(col) < v2.at(col);
}

bool CompareAsc(vector<string> v1, vector<string> v2, int col) {
	return v1.at(col) > v2.at(col);
}

bool CompareAscNum(vector<string> v1, vector<string> v2, int col) {
	int num1 = atoi(v1.at(col).c_str());
	int num2 = atoi(v2.at(col).c_str());

	return num1 > num2;
}

bool CompareDescNum(vector<string> v1, vector<string> v2, int col) {
	int num1 = atoi(v1.at(col).c_str());
	int num2 = atoi(v2.at(col).c_str());

	return num1 < num2;
}