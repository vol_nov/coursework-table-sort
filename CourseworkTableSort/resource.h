//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Resource.rc
//
#define IDC_MYICON                      2
#define IDD_CONTROLS_DLG                101
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_OOPLAB1                     107
#define IDI_SMALL                       108
#define IDC_OOPLAB1                     109
#define IDR_MAINFRAME                   128
#define IDC_OK_BTN                      1000
#define IDC_CANCEL_BTN                  1001
#define IDC_SCROLLER                    1002
#define IDC_LABEL_VAL                   1003
#define ID_32777                        32777
#define ID_32778                        32778
#define ID_32779                        32779
#define ID_32780                        32780
#define ID_OPEN                         32781
#define ID_SAVE                         32782
#define ID_SAVE_AS                      32783
#define IDM_OPEN                        32784
#define IDM_SAVE                        32785
#define IDM_SAVE_AS                     32786
#define ID_32789                        32789
#define ID_32790                        
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32794
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
