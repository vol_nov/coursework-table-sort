#pragma once
#include <Windows.h>

struct Scroll {
	int counterX, counterY;
	int dx, dy;
	int maxWidth, maxHeight;

	Scroll(HWND hWnd, int maxW, int maxH);
};

void OnVScroll(Scroll* scroll, HWND hWnd, WPARAM wParam, LPARAM lParam);
void OnHScroll(Scroll* scroll, HWND hWnd, WPARAM wParam, LPARAM lParam);